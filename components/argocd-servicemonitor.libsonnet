local ns = 'argocd';

local serviceMonitor(name) = {
  apiVersion: 'monitoring.coreos.com/v1',
  kind: 'ServiceMonitor',
  metadata: {
    labels: {
      release: 'prometheus-operator',
    },
    name: name,
    namespace: ns,
  },
  spec: {
    endpoints: [
      {
        port: 'metrics',
      },
    ],
    selector: {
      matchLabels: {
        'app.kubernetes.io/name': name,
      },
    },
  },
};

// This creates no Applications and therefore needn’t be parametrized
// with the cluster.
function() {
  metrics: serviceMonitor('argocd-metrics'),
  serverMetrics: serviceMonitor('argocd-server-metrics'),
  repoServer: serviceMonitor('argocd-repo-server'),
  applicationsetController: serviceMonitor('argocd-applicationset-controller'),
}
