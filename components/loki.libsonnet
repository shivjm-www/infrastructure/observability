local k = import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet';

local np = k.networking.v1.networkPolicy;
local i = k.networking.v1.networkPolicyIngressRule;
local peer = k.networking.v1.networkPolicyPeer;

local allSelector = {
  'app.kubernetes.io/name': 'loki',
};
local gatewaySelector = {
  'app.kubernetes.io/name': 'loki',
  'app.kubernetes.io/component': 'gateway',
};
local readSelector = {
  'app.kubernetes.io/name': 'loki',
  'app.kubernetes.io/component': 'read',
};
local writeSelector = {
  'app.kubernetes.io/name': 'loki',
  'app.kubernetes.io/component': 'write',
};

local withNs = {
  metadata+: {
    namespace: 'observability',
  },
};

function(kubernetesServer, namePrefix, replicasNumber) {
  local common = (import 'common.libsonnet')(kubernetesServer, namePrefix),
  local app = common.app,

  local replicas = std.toString(replicasNumber),

  loki:
    app('loki', {
      chart: 'loki',
      repoURL: 'https://grafana.github.io/helm-charts',
      targetRevision: '5.43.3',
      helm: {
        values: (importstr '../files/loki-values.yaml'),
        parameters: [
          {
            name: 'loki.commonConfig.replication_factor',
            value: replicas,
          },
          {
            name: 'read.replicas',
            value: replicas,
          },
          {
            name: 'read.autoscaling.maxReplicas',
            value: replicas,
          },
          {
            name: 'write.replicas',
            value: replicas,
          },
          {
            name: 'write.autoscaling.maxReplicas',
            value: replicas,
          },
        ],
        // Don’t re-install the Prometheus Operator CRDs.
        skipCrds: true,
      },
    }, [
      {
        group: 'apps',
        kind: 'StatefulSet',
        namespace: 'observability',
        name: 'loki-read',
        jsonPointers: [
          '/spec/persistentVolumeClaimRetentionPolicy',
        ],
      },
    ]),

  promtail:
    app('promtail', {
      chart: 'promtail',
      repoURL: 'https://grafana.github.io/helm-charts',
      targetRevision: '6.3.0',
      helm: {
        parameters: [
          {
            name: 'serviceMonitor.enabled',
            value: 'true',
          },
        ],
        values: (importstr '../files/promtail-values.yaml'),
      },
    }),

  // The Loki chart does provide optional NetworkPolicies but not in
  // such a way as to allow both Promtail and Grafana to access it.
  gatewayIngressNp:
    np.new('loki-gateway-ingress') +
    withNs +
    np.spec.podSelector.withMatchLabels(gatewaySelector) +
    np.spec.withIngress(
      k.networking.v1.networkPolicyIngressRule.withFrom([
        peer.podSelector.withMatchLabels({
          'app.kubernetes.io/name': 'promtail',
        }),
        peer.podSelector.withMatchLabels({
          'app.kubernetes.io/name': 'grafana-agent',
          'grafana-agent': 'loki',
        }),
        peer.podSelector.withMatchLabels({
          'app.kubernetes.io/name': 'grafana',
          'app.kubernetes.io/part-of': 'kube-prometheus',
        }),
      ])
    ) +
    np.spec.withPolicyTypes('Ingress'),

  gatewayEgressNp:
    np.new('loki-gateway-egress') +
    withNs +
    np.spec.podSelector.withMatchLabels(gatewaySelector) +
    np.spec.withEgress(
      k.networking.v1.networkPolicyEgressRule.withTo([
        peer.podSelector.withMatchLabels(writeSelector),
        peer.podSelector.withMatchLabels(readSelector),
      ] + common.linkerdEgressSelectors),
    ) +
    np.spec.withPolicyTypes('Egress'),

  metricsNp:
    np.new('loki-metrics') +
    withNs +
    np.spec.podSelector.withMatchLabels(allSelector) +
    np.spec.withIngress(
      k.networking.v1.networkPolicyIngressRule.withFrom([
        peer.podSelector.withMatchLabels({
          'app.kubernetes.io/component': 'prometheus',
          'app.kubernetes.io/part-of': 'kube-prometheus',
        }),
      ]) +
      k.networking.v1.networkPolicyIngressRule.withPorts([
        {
          port: 'http-metrics',
          protocol: 'TCP',
        },
      ])
    ) +
    np.spec.withPolicyTypes('Ingress'),

  local internalNp(c, s) =
    np.new('loki-internal-ingress-%s' % [c]) +
    withNs +
    np.spec.podSelector.withMatchLabels(s) +
    np.spec.withIngress(
      k.networking.v1.networkPolicyIngressRule.withFrom([
        peer.podSelector.withMatchLabels(allSelector),
      ])
    ) +
    np.spec.withPolicyTypes('Ingress'),

  readInternalNp: internalNp('read', readSelector),
  writeInternalNp: internalNp('write', writeSelector),
}
