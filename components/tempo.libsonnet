local k = import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet';
local np = k.networking.v1.networkPolicy;
local i = k.networking.v1.networkPolicyIngressRule;
local e = k.networking.v1.networkPolicyEgressRule;
local peer = k.networking.v1.networkPolicyPeer;

local podLabels = {
  'app.kubernetes.io/name': 'tempo',
};

function(kubernetesServer, namePrefix, localS3) {
  local common = (import './common.libsonnet')(kubernetesServer, namePrefix),
  local app = common.app,

  local withNs = {
    metadata+: {
      namespace: 'observability',
    },
  },

  local policyFor =
    function(name, component, labels)
      np.new('%s-to-tempo' % [name]) +
      withNs +
      np.spec.podSelector.withMatchLabels(podLabels) +
      np.spec.withIngress(
        i.withFrom(
          [
            peer.podSelector.withMatchLabels({
              'app.kubernetes.io/component': component,
            } + ls)
            for ls in labels
          ]
        )
      ) +
      np.spec.withPolicyTypes('Ingress'),

  // This rule also covers distributor-to-ingester traffic and
  // Prometheus remote write.
  commonIngress:
    np.new('tempo-ingress-common') +
    withNs +
    np.spec.podSelector.withMatchLabels(podLabels) +
    np.spec.withIngress(
      i.withFrom([
        peer.podSelector.withMatchLabels(podLabels),
        peer.podSelector.withMatchLabels({
          'app.kubernetes.io/part-of': 'kube-prometheus',
          'app.kubernetes.io/component': 'prometheus',
        }),
      ])
    ),

  distributorIngress:
    np.new('tempo-ingress-distributor') +
    withNs +
    np.spec.podSelector.withMatchLabels(podLabels {
      'app.kubernetes.io/component': 'distributor',
    }) +
    np.spec.withIngress(
      i.withFrom([
        peer.namespaceSelector.withMatchLabels({}),
        peer.podSelector.withMatchLabels({}),
      ])
    ),

  local queryIngressPolicy(c) =
    np.new('tempo-ingress-query-%s' % [c]) +
    withNs +
    np.spec.podSelector.withMatchLabels(podLabels {
      'app.kubernetes.io/component': c,
    }) +
    np.spec.withIngress(
      i.withFrom([
        peer.podSelector.withMatchLabels({
          'app.kubernetes.io/part-of': 'kube-prometheus',
          'app.kubernetes.io/component': 'grafana',
        }),
      ])
    ),

  querierIngress: queryIngressPolicy('querier'),
  queryFrontendIngress: queryIngressPolicy('query-frontend'),

  commonEgress:
    np.new('tempo-egress-common') +
    withNs +
    np.spec.podSelector.withMatchLabels(podLabels {
      'app.kubernetes.io/component': 'ingester',
    }) +
    np.spec.withEgress(
      [
        e.withTo([
          peer.podSelector.withMatchLabels(podLabels),
        ] + common.linkerdEgressSelectors),
      ]
    ),

  local networkCidrEgressPolicy(c) =
    np.new('tempo-egress-%s' % [c]) +
    withNs +
    np.spec.podSelector.withMatchLabels(podLabels {
      'app.kubernetes.io/component': c,
    }) +
    np.spec.withEgress(
      [
        // This presumably makes the policy ineffectual by
        // allowing all egress to the cluster as well, but at
        // least these policies limit egress from the Pods that
        // don’t access object storage.
        e.withTo([
          peer.ipBlock.withCidr('0.0.0.0/0'),
        ] + common.linkerdEgressSelectors),
      ]
    ),

  ingesterEgress: networkCidrEgressPolicy('ingester'),
  compactorEgress: networkCidrEgressPolicy('compactor'),
  querierEgress: networkCidrEgressPolicy('querier'),
  queryFrontendEgress: networkCidrEgressPolicy('query-frontend'),

  app:
    app('tempo', {
      repoURL: 'https://grafana.github.io/helm-charts',
      chart: 'tempo-distributed',
      targetRevision: '0.26.7',
      helm: {
        releaseName: 'tempo-distributed',
        values: (importstr '../files/tempo-values.yaml'),
        parameters: [
          {
            name: 'storage.trace.s3.insecure',
            value: std.toString(localS3),
          },
          {
            name: 'storage.trace.s3.forcepathstyle',
            value: std.toString(localS3),
          },
        ],
      },
    }),
}
