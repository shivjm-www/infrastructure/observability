// TODO: Add NetworkPolicies.
function(kubernetesServer, namePrefix, localS3) {
  local common = (import './common.libsonnet')(kubernetesServer, namePrefix),
  local app = common.app,

  etcd:
    app('mimir-etcd', {
      repoURL: 'https://charts.bitnami.com/bitnami',
      chart: 'etcd',
      targetRevision: '8.5.6',
      helm: {
        values: (importstr '../files/mimir-etcd-values.yaml'),
      },
    }),

  app:
    app('mimir', {
      repoURL: 'https://grafana.github.io/helm-charts',
      chart: 'mimir-distributed',
      targetRevision: '3.1.0',
      helm: {
        values: (importstr '../files/mimir-values.yaml'),
        parameters: [
          {
            name: 'mimir.structuredConfig.common.storage.s3.insecure',
            value: std.toString(localS3),
          },
        ],
      },
    }),
}
