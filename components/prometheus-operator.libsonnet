local k = import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet';
local e = k.core.v1.envVar;
local ns = 'observability';
local grafanaCredentialsSecret = 'grafana-admin-credentials';
local addMixin = (import 'kube-prometheus/lib/mixin.libsonnet');

function(kubernetesServer, namePrefix, monitoredNamespaces, prometheusReplicas) {
  local ssaAnnotation = {
    metadata+: {
      annotations+: {
        'argocd.argoproj.io/sync-options': 'ServerSideApply=true',
      },
    },
  },

  local grafanaDatasources = [
    {
      name: 'prometheus',
      type: 'prometheus',
      access: 'proxy',
      orgId: 1,
      url: 'http://mimir-nginx.' + ns + '.svc/prometheus',
      jsonData: {
        alertmanagerUid: 'alertmanager',
      },
      version: 1,
      editable: false,
      uid: 'prometheus',
    },
    {
      name: 'Loki',
      type: 'loki',
      access: 'proxy',
      orgId: 1,
      url: 'http://loki-gateway.' + ns + '.svc.cluster.local:80',
      jsonData: {
        alertmanagerUid: 'alertmanager',
      },
      version: 1,
      editable: false,
      uid: 'loki',
    },
    {
      name: 'Tempo',
      type: 'tempo',
      access: 'proxy',
      orgId: 1,
      url: 'http://tempo-distributed-query-frontend.' + ns + '.svc:3100',
      version: 1,
      editable: false,
      jsonData: {
        tracesToLogs: {
          datasourceUid: 'loki',
          tags: [
            'job',
            'instance',
            'pod',
            'namespace',
          ],
          mappedTags: [
            {
              key: 'service.name',
              value: 'service',
            },
          ],
          mapTagNamesEnabled: false,
          spanStartTimeShift: '1h',
          spanEndTimeShift: '1h',
          filterByTraceID: false,
          filterBySpanID: false,
        },
        serviceMap: {
          datasourceUid: 'prometheus',
        },
        nodeGraph: {
          enabled: 'true',
        },
        lokiSearch: {
          datasourceUid: 'loki',
        },
      },
    },
    {
      name: 'Alertmanager',
      type: 'alertmanager',
      url: 'http://mimir-nginx.' + ns + '.svc/alertmanager',
      access: 'proxy',
      uid: 'alertmanager'
    },
  ],

  local lokiMixinRulesDashboards = addMixin({
    name: 'loki',
    mixin: (import 'loki-mixin/mixin.libsonnet') + {
      _config+: {
        canary: {
          enabled: false,
        },
        ssd+: {
          enabled: true,
        },
      },
    },

    prometheusRules: lokiMixinRulesDashboards.prometheusRules {
      metadata+: k.meta.v1.objectMeta.withNamespace($.namespace),
    },
  }),

  local tempoMixinRulesDashboards = addMixin({
    name: 'tempo',
    mixin: (import 'tempo-mixin/mixin.libsonnet') + {
      _config+: {
      },
    },

    prometheusRules: tempoMixinRulesDashboards.prometheusRules {
      metadata+: k.meta.v1.objectMeta.withNamespace($.namespace),
    },
  }),

  // Workaround for tempo-mixin/mimir-mixin incompatibility.
  local mimirMixinRulesDashboards = (import './mimir-mixin-compiled.json'),

  // local mimirMixinRulesDashboards = addMixin({
  //   name: 'mimir',
  //   mixin: (import 'mimir-mixin/mixin.libsonnet') + {
  //     _config+: {
  //     },
  //   },
  //   // mixin: (import './mimir-mixin-compiled.json'),

  //   prometheusRules: mimirMixinRulesDashboards.prometheusRules {
  //     metadata+: k.meta.v1.objectMeta.withNamespace($.namespace),
  //   },
  // }),

  local kp =
    (import 'kube-prometheus/main.libsonnet') +
    (import 'kube-prometheus/addons/all-namespaces.libsonnet') +
    (import 'kube-prometheus/addons/anti-affinity.libsonnet') +
    (import 'kube-prometheus/addons/managed-cluster.libsonnet') +
    // The default NetworkPolicies don’t make much sense.
    (import 'kube-prometheus/addons/networkpolicies-disabled.libsonnet') +
    {
      values+:: {
        common+: {
          namespace: ns,

          versions+: {
            grafana: '10.2.3',
          },
        },

        prometheus+:: {
          namespaces: ['default', 'kube-system', ns] + monitoredNamespaces,

          resources: {
            requests: {
              memory: '1.5Gi',
            },
            limits: {
              memory: '3.5Gi',
            },
          },
        },

        nodeExporter+:: {
          mixin+:: {
            _config+: {
              dashboardTimezone: 'browser',
            },
          },
        },

        kubernetesControlPlane+: {
          mixin+:: {
            _config+: {
              grafanaK8s+: {
                refresh: '30s',
                grafanaTimezone: 'browser',
              },
            },
          },
        },

        grafana+:: {
          plugins: ['grafana-polystat-panel', 'grafana-piechart-panel'],

          mixin+: {
            _config+: {
            },
          },

          datasources: grafanaDatasources,

          dashboards:
            std.mergePatch(super.dashboards, {
              // These dashboards are unusable with managed clusters.
              'controller-manager.json': null,
              'proxy.json': null,
              'apiserver.json': null,
              'scheduler.json': null,
            }),

          rawDashboards+:: {
            // https://grafana.com/grafana/dashboards/11001-cert-manager/
            'cert-manager.json': (importstr '../files/dashboards/cert-manager.json'),
            'cert-manager-2.json': (importstr '../files/dashboards/cert-manager-2.json'),
            // https://github.com/argoproj/argo-cd/blob/192bc5093ad4b410b84d23b70ea953f0d60512ab/examples/dashboard.json
            'argocd.json': (importstr '../files/dashboards/argocd.json'),
            'tasks.json': (importstr '../files/dashboards/tasks.json'),
            // TODO: Add dashboards for Argo CD ApplicationSets and
            // notifications.
          },

          folderDashboards+:: (import 'dashboards.libsonnet') + {
            'Loki': lokiMixinRulesDashboards.grafanaDashboards,
            'Tempo': tempoMixinRulesDashboards.grafanaDashboards,
            'Mimir': mimirMixinRulesDashboards.grafanaDashboards,
          },
          folderUidGenerator:: std.md5,

          env: [
            e.fromSecretRef('GF_SECURITY_ADMIN_USER', grafanaCredentialsSecret, 'user'),
            e.fromSecretRef('GF_SECURITY_ADMIN_PASSWORD', grafanaCredentialsSecret, 'password'),
          ],

          config+: {
            sections: {
              log: {
                mode: 'console',

                console: {
                  format: 'json',
                },
              },

              users: {
                default_theme: 'light',
              },

              feature_toggles: {
                enable: 'tempoSearch tempoBackendSearch',
              },
            },
          },
        },
      },

      prometheus+:: {
        prometheus+: {
          spec+: {
            // These values are required for deduplication.
            replicaExternalLabelName: '__replica__',
            prometheusExternalLabelName: 'cluster',

            replicas: prometheusReplicas,

            remoteWrite: [
              {
                url: 'http://mimir-nginx.' + ns + '.svc/api/v1/push',
              },
            ],
          },
        },
      },
    },

  components:
    [kp.kubePrometheus[name] for name in std.filter(function(name) name != 'namespace', std.objectFields(kp.kubePrometheus))] +
    // Exclude CRDs (installed in base Application).
    [kp.prometheusOperator[name] for name in std.filter(function(name) !std.startsWith(name, '0'), std.objectFields(kp.prometheusOperator))] +
    [kp.nodeExporter[name] for name in std.objectFields(kp.nodeExporter)] +
    [kp.kubeStateMetrics[name] for name in std.objectFields(kp.kubeStateMetrics)] +
    [kp.prometheus[name] for name in std.objectFields(kp.prometheus)] +
    [kp.prometheusAdapter[name] for name in std.objectFields(kp.prometheusAdapter)] +
    [kp.grafana.dashboardDefinitions {
      items: [item + ssaAnnotation for item in kp.grafana.dashboardDefinitions.items],
    }] +
    [kp.grafana[name] for name in std.filter(function(name) name != 'dashboardDefinitions', std.objectFields(kp.grafana))] +
    [kp.kubernetesControlPlane[name] for name in std.objectFields(kp.kubernetesControlPlane)] +
    [r {
      metadata+: {
        namespace: ns,
      },
    } for r in [
      lokiMixinRulesDashboards.prometheusRules,
      tempoMixinRulesDashboards.prometheusRules,
      mimirMixinRulesDashboards.prometheusRules,
    ]],
}
