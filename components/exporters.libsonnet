function(kubernetesServer, namePrefix) {
  local app = (import 'common.libsonnet')(kubernetesServer, namePrefix).app,

  gcpe:
    app('gitlab-ci-pipelines-exporter', {
      chart: 'gitlab-ci-pipelines-exporter',
      repoURL: 'https://charts.visonneau.fr',
      targetRevision: '0.2.18',
      helm: { values: (importstr '../files/gitlab-ci-pipelines-exporter-values.yaml') },
    }),
}
