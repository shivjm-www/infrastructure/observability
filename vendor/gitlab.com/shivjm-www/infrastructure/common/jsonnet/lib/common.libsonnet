local argo = import 'github.com/jsonnet-libs/argo-cd-libsonnet/2.9/main.libsonnet';
local a = argo.argoproj.v1alpha1.application;
local spec = a.spec;
local p = argo.argoproj.v1alpha1.appProject;

local finalizers = [
  'resources-finalizer.argocd.argoproj.io',
];

local syncPolicy =
  a.spec.syncPolicy.automated.withAllowEmpty(false) +
  a.spec.syncPolicy.automated.withPrune(true) +
  a.spec.syncPolicy.automated.withSelfHeal(false);

local syncOptions(createNamespace=false, ssa=false, includeCrds=false) =
  a.spec.syncPolicy.withSyncOptions([
    'CreateNamespace=' + createNamespace,
    'PrunePropagationPolicy=foreground',
    'PruneLast=true',
    'SkipDryRunOnMissingResource=' + includeCrds,
  ] + if ssa then ['ServerSideApply=true'] else []);

local crdSyncOptions(ssa=false) = {
  'argocd.argoproj.io/sync-options': 'SkipDryRunOnMissingResource=true' + if ssa then ' ServerSideApply=true' else '',
};

local linkerdInjectAnnotation(enabled=true) = {
  'linkerd.io/inject': if enabled then 'enabled' else 'disabled',
};

local syncWaveAnnotation = 'argocd.argoproj.io/sync-wave';

function(cluster, namePrefix) {
  local destinationAttribute = if std.startsWith(cluster, 'https://') then { server: cluster } else { name: cluster },

  syncWaveAnnotation: syncWaveAnnotation,
  hasSyncWave(r):
    std.isObject(r.metadata) &&
    std.objectHas(r.metadata, 'annotations') &&
    std.isObject(r.metadata.annotations)
    && std.objectHas(r.metadata.annotations, syncWaveAnnotation),
  a: a,
  p: p,
  spec: spec,
  app(name, project, namespace, source, ignoreDifferences=[], destination=cluster):
    a.new(namePrefix + name) +
    a.metadata.withFinalizers(finalizers) +
    a.metadata.withNamespace('argocd')
    + {
      spec: {
        destination: destinationAttribute,
      },
    }
    + spec.destination.withNamespace(namespace)
    + spec.syncPolicy.withSyncOptionsMixin([
      'CreateNamespace=false',
      'PrunePropagationPolicy=foreground',
      'PruneLast=true',
    ]) +
    (if std.length(ignoreDifferences) > 0 then spec.withIgnoreDifferencesMixin(ignoreDifferences) else {}) +
    spec.withProject(project) +
    spec.syncPolicy.automated.withAllowEmpty(false) +
    spec.syncPolicy.automated.withSelfHeal(false) +
    spec.syncPolicy.automated.withPrune(true) +
    {
      spec+:
        {
          source: source,
        },
    },
  project(name, description, sourceRepos, namespaces):
    p.new(name) +
    p.metadata.withFinalizers(finalizers) +
    p.metadata.withNamespace('argocd') +
    p.spec.withDestinations(
      std.map(function(ns) { namespace: ns, server: 'https://kubernetes.default.svc' }, namespaces) +
      std.map(function(ns) { namespace: ns, name: 'ugc' }, namespaces),
    ) +
    p.spec.withSourceRepos(sourceRepos) +
    p.spec.withDescription(description),
  sw(n): a.metadata.withAnnotationsMixin({
    [syncWaveAnnotation]: std.toString(n),
  }),
  helmParameters(params): [
    {
      name: p[0],
      value: p[1],
    } + if std.length(p) > 2 then { forceString: true } else {}
    for p in params
  ],
  syncPolicy: syncPolicy,
  syncOptions: syncOptions,
  crdSyncOptions: crdSyncOptions,
  linkerdInjectAnnotation: linkerdInjectAnnotation,
  linkerdInject(enabled=true): {
    metadata+: {
      annotations+: linkerdInjectAnnotation(enabled),
    },
  },
  contourRateLimiting: {
    spec+: {
      virtualhost+: {
        rateLimitPolicy: {
          'local': {
            requests: 20,
            unit: 'second',
            burst: 50,
          },
        },
      },
    },
  },
  tlsConfig: {
    spec+: {
      virtualhost+: {
        tls+: {
          secretName: 'contour/shivjm-in-tls',
        },
      },
    },
  },
}
