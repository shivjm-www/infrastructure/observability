function (kubernetesServer, namePrefix, monitoredNamespaces, prometheusReplicas, lokiReplicas, localS3)
  std.objectValues((import "../components/mimir.libsonnet")(kubernetesServer, namePrefix, localS3)) +
  (import "../components/prometheus-operator.libsonnet")(kubernetesServer, namePrefix, monitoredNamespaces, prometheusReplicas).components +
  std.objectValues((import "../components/loki.libsonnet")(kubernetesServer, namePrefix, lokiReplicas)) +
  std.objectValues((import "../components/tempo.libsonnet")(kubernetesServer, namePrefix, localS3)) +
  std.objectValues((import "../components/exporters.libsonnet")(kubernetesServer, namePrefix)) +
  std.objectValues((import "../components/argocd-servicemonitor.libsonnet")())
