local AddPrometheusDatasource(dashboard) = dashboard {
  __inputs: std.filter(function(i) i.name != 'DS_PROMETHEUS', dashboard.__inputs),
  templating+: {
    list: [
      {
        current: {
          selected: true,
          text: 'Prometheus',
          value: 'Prometheus',
        },
        description: null,
        'error': null,
        hide: 0,
        includeAll: false,
        label: 'Datasource',
        multi: false,
        name: 'DS_PROMETHEUS',
        options: [],
        query: 'prometheus',
        queryValue: '',
        refresh: 1,
        regex: '',
        skipUrlSync: false,
        type: 'datasource',
      },
    ] + dashboard.templating.list,
  },
};

local ConvertLinkerdDashboard(raw) = AddPrometheusDatasource(raw);

{
  // https://github.com/mvisonneau/gitlab-ci-pipelines-exporter/tree/107681391ce32e97d9efd485e07037bc3f594c46/examples/quickstart/grafana/dashboards
  'GitLab CI': {
    'gitlab-ci-jobs.json': (import '../files/dashboards/gitlab-ci/jobs.json'),
    'gitlab-ci-pipelines.json': (import '../files/dashboards/gitlab-ci/pipelines.json'),
    'gitlab-ci-environments-and-deployments.json': (import '../files/dashboards/gitlab-ci/environments-and-deployments.json'),
  },

  Observability: {
    'logging-dashboard-via-loki.json': (import '../files/dashboards/monitoring/logging-dashboard-via-loki.json'),
  },

  Web: {
    // https://grafana.com/grafana/dashboards/15038-external-dns/
    'external-dns.json': (import '../files/dashboards/web/external-dns_rev1.json'),
    // https://github.com/projectcontour/contour/blob/88151401409e9ece7944dd8aa81573d887dd7bdd/examples/grafana/02-grafana-configmap.yaml
    'contour.json': AddPrometheusDatasource(import '../files/dashboards/web/contour.json'),
  },

  Linkerd: {
    'l5d-authority.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/authority.json'),
    'l5d-cronjob.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/cronjob.json'),
    'l5d-daemonset.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/daemonset.json'),
    'l5d-deployment.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/deployment.json'),
    'l5d-health.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/health.json'),
    'l5d-job.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/job.json'),
    'l5d-multicluster.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/multicluster.json'),
    'l5d-namespace.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/namespace.json'),
    'l5d-pod.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/pod.json'),
    'l5d-replicaset.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/replicaset.json'),
    'l5d-replicationcontroller.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/replicationcontroller.json'),
    'l5d-route.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/route.json'),
    'l5d-service.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/service.json'),
    'l5d-statefulset.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/statefulset.json'),
    'l5d-top-line.json': ConvertLinkerdDashboard(import '../files/dashboards/linkerd/top-line.json'),
  },
}
