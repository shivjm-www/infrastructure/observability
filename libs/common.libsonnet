local argo = import 'github.com/jsonnet-libs/argo-cd-libsonnet/2.9/main.libsonnet';
local k = import 'github.com/jsonnet-libs/k8s-libsonnet/1.26/main.libsonnet';
local servicesCommon = import 'gitlab.com/shivjm-www/infrastructure/common/jsonnet/lib/common.libsonnet';
local argoApp = argo.argoproj.v1alpha1.application;
local spec = argoApp.spec;
local peer = k.networking.v1.networkPolicyPeer;

function(server, namePrefix) servicesCommon(server, namePrefix) + {
  app(name, source, ignoreDifferences=[]):
    servicesCommon(server, namePrefix).app(name, 'observability', 'observability', source, ignoreDifferences) + spec.destination.withServer('https://kubernetes.default.svc'),
  linkerdEgressSelectors: [
    peer.namespaceSelector.withMatchLabels({
      'kubernetes.io/metadata.name': 'linkerd',
    }),
    peer.namespaceSelector.withMatchLabels({
      'linkerd.io/extension': 'jaeger',
    }),
    peer.namespaceSelector.withMatchLabels({
      'kubernetes.io/metadata.name': 'kube-system',
    }),
  ],
}
