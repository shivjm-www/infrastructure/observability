#!/bin/sh

# based on <https://gist.githubusercontent.com/jirutka/afa3ce62b1430abf7572/raw/8b7decc45adb634dd6d3e71b7cb096f1fbb89ac8/pg_grant_read_to_db.sh>
#
# The MIT License
#
# Copyright 2014-2017 Jakub Jirutka <jakub@jirutka.cz>.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

DB_NAME='postgres'
ROLE='grafana'
SCHEMA='public'

pgexec() {
	local cmd=$1
	psql --no-psqlrc --no-align --tuples-only --record-separator=\0 --quiet \
		--echo-queries --command="$cmd" "$DB_NAME"
}

pgexec "
CREATE ROLE $ROLE WITH PASSWORD 'plaintext' NOINHERIT LOGIN;
GRANT CONNECT ON DATABASE $DB_NAME TO $ROLE;
GRANT USAGE ON SCHEMA $SCHEMA TO $ROLE;
GRANT SELECT ON ALL TABLES IN SCHEMA $SCHEMA TO $ROLE;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA $SCHEMA TO $ROLE;
ALTER DEFAULT PRIVILEGES IN SCHEMA $SCHEMA GRANT SELECT ON TABLES TO $ROLE;
ALTER DEFAULT PRIVILEGES IN SCHEMA $SCHEMA GRANT SELECT ON SEQUENCES TO $ROLE;"
