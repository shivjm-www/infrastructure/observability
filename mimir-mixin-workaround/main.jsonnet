local addMixin = (import 'kube-prometheus/lib/mixin.libsonnet');
local mm = (import 'github.com/grafana/mimir/operations/mimir-mixin/mixin.libsonnet');

addMixin({
  name: 'mimir',
  mixin: (import 'mimir-mixin/mixin.libsonnet') + {
    _config+: {
      dashboard_prefix: '',
    },
  },
})
